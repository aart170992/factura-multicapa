﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using accesoADatos;

namespace negocio.factura
{
    public class AdministradorDeFacturas 
    {
        protected string cadenaDeConexionInterna = "";
        protected int tipoDeBaseDeDatos;

        public AdministradorDeFacturas(string cadenaDeConexion, int tipoBaseDatos) {
            cadenaDeConexionInterna = cadenaDeConexion;
            tipoDeBaseDeDatos = tipoBaseDatos;
        }

        public FacturaNegocio crearFactura(FacturaNegocio factura)
        {
            int idFactura = -1;
            accesoADatos.repositorios.MSSQLRepositorioCabeceraFactura repositorioCabecera = new accesoADatos.repositorios.MSSQLRepositorioCabeceraFactura(cadenaDeConexionInterna);
            accesoADatos.repositorios.MSSQLRepositorioDetalleFactura repositorioDetalle = new accesoADatos.repositorios.MSSQLRepositorioDetalleFactura(cadenaDeConexionInterna);
            accesoADatos.clases.CabeceraFactura cabecera = new accesoADatos.clases.CabeceraFactura();

            cabecera.fac_Id = 0;
            cabecera.fac_Fecha = factura.fecha;
            cabecera.fac_Cli_Id = factura.idCliente;
            cabecera.fac_Subtotal = factura.subtotal;
            cabecera.fac_iva = factura.iva;
            cabecera.fac_total = factura.total;
            cabecera.fac_numero = factura.numeroFactura;

            
            idFactura = repositorioCabecera.insertarCabecera(cabecera);

            foreach (var item in factura.detalles)
            {
                accesoADatos.clases.DetalleFactura detalleFactura = new accesoADatos.clases.DetalleFactura {
                    det_Fac_Id = idFactura,
                    det_Pro_Id=item.idProducto,
                    det_Pro_Precio=item.precio,
                    det_Cantidad=item.cantidad,
                    det_Total_Item=item.totalItem

                };
                item.id= repositorioDetalle.ingresarDetalle(detalleFactura);
                item.idFactura = idFactura;
            }

            factura.id = idFactura;
            return factura;

        }

        public List<FacturaNegocio> ObtenerFacturaPorClientes(int idCliente) {
            accesoADatos.repositorios.MSSQLRepositorioCabeceraFactura repositorioCabecera = new accesoADatos.repositorios.MSSQLRepositorioCabeceraFactura(cadenaDeConexionInterna);
            accesoADatos.repositorios.MSSQLRepositorioDetalleFactura repositorioDetalle = new accesoADatos.repositorios.MSSQLRepositorioDetalleFactura(cadenaDeConexionInterna);

            List<FacturaNegocio> facturas = new List<FacturaNegocio>();

            var cabeceras = repositorioCabecera.obtenerCabeceraFacturaCLiente(idCliente);
            foreach (var cabecera in cabeceras)
            {
                FacturaNegocio FacturaEncontrada = new FacturaNegocio {
                    id = cabecera.fac_Id,
                    idCliente=cabecera.fac_Cli_Id,
                    fecha=cabecera.fac_Fecha,
                    subtotal=cabecera.fac_Subtotal,
                    iva=cabecera.fac_iva,
                    total=cabecera.fac_total,
                    numeroFactura=cabecera.fac_numero

                };
                var detalles =repositorioDetalle.obtenerDetallePorFactura(cabecera.fac_Id);
                FacturaEncontrada.detalles = new List<DetalleFaturaNegocio>();
                foreach (var detalle in detalles)
                {
                    DetalleFaturaNegocio detallesFactura = new DetalleFaturaNegocio {
                        id = detalle.det_id,
                        idFactura = detalle.det_Fac_Id,
                        idProducto = detalle.det_Pro_Id,
                        precio=detalle.det_Pro_Precio,
                        cantidad=detalle.det_Cantidad,
                        totalItem=detalle.det_Total_Item
                        
                    };
                    FacturaEncontrada.detalles.Add(detallesFactura);


                }
                facturas.Add(FacturaEncontrada);



            }

            return facturas;
        }

        public ClienteFactura buscarClienteXCedula(string cedula) {
            accesoADatos.repositorios.MSSQLRepositorioCliente metodosCliente = new accesoADatos.repositorios.MSSQLRepositorioCliente(cadenaDeConexionInterna);
            accesoADatos.clases.Cliente cliente = new accesoADatos.clases.Cliente();
            
            cliente = metodosCliente.buscarCliente(cedula);
            ClienteFactura clienteFactura = new ClienteFactura {
                Cli_ID = cliente.Cli_ID,
                cli_Cedula=cliente.cli_Cedula,
                cli_Nombre=cliente.cli_Nombre,
                cli_Apellido=cliente.cli_Apellido,
                cli_Direccion=cliente.cli_Direccion,
                cli_Celular=cliente.cli_Celular,
                cli_Correo=cliente.cli_Correo

            };
            return clienteFactura;
        }

        public ProductoFactura BuscarProductoXId(int idProducto) {

            //accesoADatos.repositorios.MSSQLRepositorioProducto metodosProductos = new accesoADatos.repositorios.MSSQLRepositorioProducto(cadenaDeConexionInterna);
            accesoADatos.repositorios.IRepository<accesoADatos.clases.Producto> metodosProducto = accesoADatos.repositorios.fabrica.FabricaProductoRepositorio.ObtenerRepository(cadenaDeConexionInterna, tipoDeBaseDeDatos);
            var producto = metodosProducto.obtenerPorId(idProducto);
            ProductoFactura datoProduco = new ProductoFactura {
                pro_Id = producto.pro_Id,
                pro_Nombre=producto.pro_Nombre,
                pro_Descripcion=producto.pro_Descripcion,
                pro_Precio=producto.pro_Precio,
                pro_Stock=producto.pro_Stock

            };
            return datoProduco;
        }
    }
}
