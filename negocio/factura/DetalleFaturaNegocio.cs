﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace negocio.factura
{
    public class DetalleFaturaNegocio
    {
        public int id { get; set; }
        public int idProducto { get; set; }
        public int idFactura { get; set; }
        public float precio { get; set; }
        public int cantidad { get; set; }
        public float totalItem { get; set; }
    }
}
