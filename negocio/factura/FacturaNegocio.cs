﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace negocio.factura
{
    public class FacturaNegocio
    {
        public int id { get; set; }
        public int idCliente { get; set; }
        public DateTime fecha { get; set; }
        public float subtotal { get; set; }
        public float iva { get; set; }
        public float total { get; set; }
        public string numeroFactura { get; set; }
        public List<DetalleFaturaNegocio> detalles { get; set; }
    }
}
