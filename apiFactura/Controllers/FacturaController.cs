﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;

namespace apiFactura.Controllers

{
    [EnableCors(origins: "http://localhost:4401", headers: "*", methods: "*")]
   
    public class FacturaController : ApiController
    {
        // GET: api/Factura
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Factura/5
        [ActionName("Fact")]
        public List<negocio.factura.FacturaNegocio> Get(int id)
        {
            List<negocio.factura.FacturaNegocio> facturas = new List<negocio.factura.FacturaNegocio>();
            negocio.factura.AdministradorDeFacturas administradorDeFacturas = new negocio.factura.AdministradorDeFacturas(WebConfigurationManager.AppSettings["cadenaDeconexionSQLSERVER"],int.Parse(WebConfigurationManager.AppSettings["tipoDeBaseDeDatos"]));
            facturas = administradorDeFacturas.ObtenerFacturaPorClientes(id);
            return facturas;
        }


        [ActionName("porCedula")]
        //GET: api/Factura/poCedula/string
        public negocio.factura.ClienteFactura Get(string cedula){
            
            negocio.factura.ClienteFactura datosCliente = new negocio.factura.ClienteFactura();
            negocio.factura.AdministradorDeFacturas administradorDeFacturas = new negocio.factura.AdministradorDeFacturas(WebConfigurationManager.AppSettings["cadenaDeconexionSQLSERVER"], int.Parse(WebConfigurationManager.AppSettings["tipoDeBaseDeDatos"]));
            datosCliente = administradorDeFacturas.buscarClienteXCedula(cedula);
            return datosCliente;
        }

        [ActionName("buscarProducto")]
        //GET: api/Factura/buscarProducto/string
        public negocio.factura.ProductoFactura GetProducto(int idProducto) {
            negocio.factura.ProductoFactura datoProducto = new negocio.factura.ProductoFactura();
            negocio.factura.AdministradorDeFacturas administradorDeFacturas = new negocio.factura.AdministradorDeFacturas(WebConfigurationManager.AppSettings["cadenaDeconexionSQLSERVER"],int.Parse(WebConfigurationManager.AppSettings["tipoDeBaseDeDatos"]));
            datoProducto = administradorDeFacturas.BuscarProductoXId(idProducto);

            return datoProducto;
        }

        // POST: api/Factura
        public negocio.factura.FacturaNegocio Post([FromBody] negocio.factura.FacturaNegocio datosFactura)
        {
            negocio.factura.FacturaNegocio facturaIngresada = new negocio.factura.FacturaNegocio();
            negocio.factura.AdministradorDeFacturas administradorDeFacturas = new negocio.factura.AdministradorDeFacturas(WebConfigurationManager.AppSettings["cadenaDeconexionSQLSERVER"], int.Parse(WebConfigurationManager.AppSettings["tipoDeBaseDeDatos"]));
            facturaIngresada = administradorDeFacturas.crearFactura(datosFactura);
            return facturaIngresada;

        }

        // PUT: api/Factura/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Factura/5
        public void Delete(int id)
        {
        }
    }
}
