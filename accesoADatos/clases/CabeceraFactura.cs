﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace accesoADatos.clases
{
    public class CabeceraFactura
    {
        public int fac_Id { get; set; }
        public int fac_Cli_Id { get; set; }
        public DateTime fac_Fecha { get; set; }
        public float fac_Subtotal { get; set; }
        public float fac_iva { get; set; }
        public float fac_total { get; set; }
        public string fac_numero { get; set; }
    }
}
