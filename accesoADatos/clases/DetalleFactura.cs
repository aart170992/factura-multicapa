﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace accesoADatos.clases
{
    public class DetalleFactura
    {
        public int det_id { get; set; }
        public int det_Pro_Id { get; set; }
        public int det_Fac_Id { get; set; }
        public float det_Pro_Precio { get; set; }
        public int det_Cantidad { get; set; }
        public float det_Total_Item { get; set; }
    }
}
