﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace accesoADatos.repositorios
{
    public class MSSQLRepositorioCabeceraFactura : core.MSSQLAdministradorDeConexion
    {
        public MSSQLRepositorioCabeceraFactura(string cadenaDeConexion) : base(cadenaDeConexion)
        {

        }

        public int insertarCabecera(clases.CabeceraFactura factura)
        {
            int idFactura = -1;
            using (var connection = new SqlConnection(this.cadenaDeConexionInterna))
            {
                try
                {
                    connection.Open();
                    string script = "INSERT INTO factura(fac_Cli_Id, fac_Fecha, fac_Subtotal, fac_iva, fac_total,fac_numero)VALUES(@cedula,@fecha,@subtotal,@iva,@total,@numero_fac); select @@identity as ID";
                    SqlCommand cmd = new SqlCommand(script, connection);
                    cmd.Parameters.AddWithValue("@cedula", factura.fac_Cli_Id);
                    cmd.Parameters.AddWithValue("@fecha", factura.fac_Fecha);
                    cmd.Parameters.AddWithValue("@subtotal", factura.fac_Subtotal);
                    cmd.Parameters.AddWithValue("@iva", factura.fac_iva);
                    cmd.Parameters.AddWithValue("@total", factura.fac_total);
                    cmd.Parameters.AddWithValue("@numero_fac", factura.fac_numero);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        idFactura = int.Parse(reader["ID"].ToString());
                    }

                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return idFactura;
        }

        public List<clases.CabeceraFactura> obtenerCabeceraFacturaCLiente(int idCliente)
        {
            List<clases.CabeceraFactura> facturasCliente = new List<clases.CabeceraFactura>();
            using (var connection = new SqlConnection(this.cadenaDeConexionInterna))
            {
                try
                {
                    connection.Open();
                    string script = $"select fac_Id, fac_Cli_Id, fac_Fecha, fac_Subtotal, fac_iva, fac_total,fac_numero from factura where fac_Cli_Id={idCliente}";
                    SqlCommand cmd = new SqlCommand(script, connection);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        clases.CabeceraFactura factura = new clases.CabeceraFactura
                        {
                            fac_Cli_Id = int.Parse(reader["fac_Cli_Id"].ToString()),
                            fac_Fecha = Convert.ToDateTime(reader["fac_Fecha"].ToString()),
                            fac_Subtotal = float.Parse(reader["fac_Subtotal"].ToString()),
                            fac_iva = float.Parse(reader["fac_iva"].ToString()),
                            fac_total = float.Parse(reader["fac_total"].ToString()),
                            fac_numero = reader["fac_numero"].ToString(),
                            fac_Id = int.Parse(reader["fac_Id"].ToString())
                        };
                        facturasCliente.Add(factura);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return facturasCliente;

        }
    }
}
