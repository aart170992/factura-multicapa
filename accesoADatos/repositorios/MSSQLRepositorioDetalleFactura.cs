﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace accesoADatos.repositorios
{
    public class MSSQLRepositorioDetalleFactura : core.MSSQLAdministradorDeConexion
    {
        public MSSQLRepositorioDetalleFactura(string cadenaDeconexion) : base(cadenaDeconexion) {
        }

        public int ingresarDetalle(clases.DetalleFactura detalle)
        {
            int idDetalle = -1;
            using (var connection = new SqlConnection(this.cadenaDeConexionInterna))
            {
                try
                {
                    connection.Open();
                    string script = "INSERT INTO detalle([det_Pro_Id], [det_Fac_Id], [det_Pro_Precio] , [det_Cantidad], [det_Total_Item]) VALUES(@idProducto, @idFactura, @proPrecio, @cantidad, @totalItem); select @@identity as ID";

                    SqlCommand cmd = new SqlCommand(script, connection);
                    cmd.Parameters.AddWithValue("@idProducto", detalle.det_Pro_Id);
                    cmd.Parameters.AddWithValue("@idFactura", detalle.det_Fac_Id);
                    cmd.Parameters.AddWithValue("@proPrecio", detalle.det_Pro_Precio);
                    cmd.Parameters.AddWithValue("@cantidad", detalle.det_Cantidad);
                    cmd.Parameters.AddWithValue("@totalItem", detalle.det_Total_Item);


                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        idDetalle = int.Parse(reader["ID"].ToString());
                    }

                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return idDetalle;

        }

        public List<clases.DetalleFactura> obtenerDetallePorFactura(int idFactura)
        {
            List<clases.DetalleFactura> detalleFacturas = new List<clases.DetalleFactura>();
            using (var connection = new SqlConnection(this.cadenaDeConexionInterna))
            {
                try
                {
                    connection.Open();
                    string script = $"select [det_Id],[det_Pro_Id], [det_Fac_Id], [det_Pro_Precio] , [det_Cantidad], [det_Total_Item] from detalle where det_Fac_Id={idFactura}";
                    SqlCommand cmd = new SqlCommand(script, connection);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        clases.DetalleFactura detalleFactura = new clases.DetalleFactura
                        {
                            det_id= int.Parse(reader["det_Id"].ToString()),
                            det_Pro_Id= int.Parse(reader["det_Pro_Id"].ToString()),
                            det_Fac_Id = int.Parse(reader["det_Fac_Id"].ToString()),
                            det_Pro_Precio= float.Parse(reader["det_Pro_Precio"].ToString()),
                            det_Cantidad= int.Parse(reader["det_Cantidad"].ToString()),
                            det_Total_Item= float.Parse(reader["det_Total_Item"].ToString())

                        };
                        detalleFacturas.Add(detalleFactura);
                        
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return detalleFacturas;

        }


    }


}
