﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace accesoADatos.repositorios.core
{
    public class MSSQLAdministradorDeConexion
    {
        protected string cadenaDeConexionInterna { get; set; }
        public MSSQLAdministradorDeConexion(string cadenaDeConexion)
        {
            cadenaDeConexionInterna = cadenaDeConexion;
        }
    }

}
